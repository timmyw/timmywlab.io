---
title: Turning off terminal echo in OpenBSD (using assembly and syscalls)
author: Tim Whelan
tags: assembly, syscalls, OpenBSD
---

#+TITLE: Turning off terminal echo in OpenBSD (using assembly and syscalls)
#+OPTIONS: toc:nil num:nil

Started recently working on simple terminal based editor written in
assembly language.  My after hours laptop runs OpenBSD, so the first
run at the editor would be on that OS.

Setup for the editor is to prepare the terminal, including turning off
keypresses echoing to the terminal.

Turning off echo in a *nix terminal usual involves the use of
~tcgetattr~ to retrieve the current terminal attributes for ~STDIN~,
masking the ~ECHO~ bit off one of them, and then using ~tcsetattr~ to
set the attributes again.

* Tracking down what ~tcgetattr~ actually does

Using the OpenBSD source, I managed to find, in ~sys/sys/termios.h~:
#+begin_src c
int	tcgetattr(int, struct termios *);
#+end_src
And in ~lib/libc/termios/tcgetattr.c~:
#+begin_src c
int
tcgetattr(int fd, struct termios *t)
{
	return (ioctl(fd, TIOCGETA, t));
}
#+end_src

So ~tcgetattr~ is implemented in terms in ~ioctl~, which is a system
call in itself.  Progress!

Next up to track down the definition of ~TIOCGETA~ to see what value it actually has.

In ~sys/ttycom.h:80~, ~TIOCGETA~ is defined as
#+begin_src c
#define TIOCGETA        _IOR('t', 19, struct termios) /* get termios struct */
#+end_src

So we need to track down the ~_IOR~ macro, but I couldn't find any
definition in the ~src~ directory at all.

After some google research, I found the following for Linux
#+begin_src c
#ifndef _IOC_SIZEBITS
# define _IOC_SIZEBITS  14
#endif

#ifndef _IOC_DIRBITS
# define _IOC_DIRBITS   2
#endif

#define _IOC_NRSHIFT    0
#define _IOC_TYPESHIFT  (_IOC_NRSHIFT+_IOC_NRBITS)
#define _IOC_SIZESHIFT  (_IOC_TYPESHIFT+_IOC_TYPEBITS)
#define _IOC_DIRSHIFT   (_IOC_SIZESHIFT+_IOC_SIZEBITS)

#ifndef _IOC_READ
# define _IOC_READ      2U
#endif

#define _IOC(dir,type,nr,size) \
        (((dir)  << _IOC_DIRSHIFT) | \
         ((type) << _IOC_TYPESHIFT) | \
         ((nr)   << _IOC_NRSHIFT) | \
         ((size) << _IOC_SIZESHIFT))

#define _IOR(type,nr,size)
  _IOC(_IOC_READ,(type),(nr),(_IOC_TYPECHECK(size)))  
#+end_src

For OpenBSD I have found some of the defines in
~gnu/llvm/compiler-rt/lib/sanitizer_common/sanitizer_platform_limits_netbsd.h~
(as below), but not the elusive ~_IOR~ macro

#+begin_src c
#define IOC_NRBITS 8
#define IOC_TYPEBITS 8
#define IOC_SIZEBITS 14
#define IOC_DIRBITS 2
#define IOC_NONE 0U
#define IOC_WRITE 1U
#define IOC_READ 2U
#define IOC_NRMASK ((1 << IOC_NRBITS) - 1)
#define IOC_TYPEMASK ((1 << IOC_TYPEBITS) - 1)
#define IOC_SIZEMASK ((1 << IOC_SIZEBITS) - 1)
#undef IOC_DIRMASK
#define IOC_DIRMASK ((1 << IOC_DIRBITS) - 1)
#define IOC_NRSHIFT 0
#define IOC_TYPESHIFT (IOC_NRSHIFT + IOC_NRBITS)
#define IOC_SIZESHIFT (IOC_TYPESHIFT + IOC_TYPEBITS)
#define IOC_DIRSHIFT (IOC_SIZESHIFT + IOC_SIZEBITS)

#define IOC_DIR(nr) (((nr) >> IOC_DIRSHIFT) & IOC_DIRMASK)
#define IOC_TYPE(nr) (((nr) >> IOC_TYPESHIFT) & IOC_TYPEMASK)
#define IOC_NR(nr) (((nr) >> IOC_NRSHIFT) & IOC_NRMASK)
#define IOC_SIZE(nr) (((nr) >> IOC_SIZESHIFT) & IOC_SIZEMASK)
  
#+end_src

There is no definition of ~_IOR~ in that header file though

Using ~grep -rn '\<_IOR\>' * > ~/ior.txt~ to trawl through all of the
source I found a lot of mentions of ~_IOR~, but only the definition of
other macros defined in terms of it.

Time to switch tack here.  I used the code snippet in ~work.c~ below
to retrieve the current set of termio attributes, and print out a
subset of them (to use later to see if I ever get this working).

#+begin_src c
#include <termios.h>
#include <stdio.h>

int main() {
  struct termios t;
  tcgetattr(0, &t);
  printf("c_iflag = %08x\nc_oflag = %08x\nc_cflag = %08x\nc_lflag = %08x\n",
	 t.c_iflag, t.c_oflag, t.c_cflag, t.c_lflag);
}
#+end_src

For validating later - this currently outputs:
#+begin_src sh
rubberduck:~/code/oscar/work (trunk) > ./work 
c_iflag = 00000300
c_oflag = 00000007
c_cflag = 00005b00
c_lflag = 200005cf
#+end_src

I ran that through ~ktrace~ and ~kdump~
#+begin_src sh
    ktrace -t c ./work
    kdump -t c -n
#+end_src

I found (snipped):

#+begin_src c
  ...
  30470 work     CALL  ioctl(0,0x402c7413,0x7f7ffffcdc28)
  ...
#+end_src

So, assuming we know how it was calculated (the assumption being that
the ~_LOR~ macro on Linux is close to what it is on OpenBSD, we should
be able to reverse engineer it

The 32 bits of the value generated by ~_LOR~ are as follows

|-------+-------+--------|
| Field | Start | Length |
|-------+-------+--------|
| NR    |     0 |      8 |
| TYPE  |     8 |      8 |
| SIZE  |    16 |     14 |
| DIR   |    30 |      2 |

Using ~0x402c7413~, our ~NR~ value will be ~0x402c7413 & 0xff~ which
is ~0x13~.  Following a similar process for the remaining fields we
have:

|------+------------+------------+-----+-------------+------------------|
| Name | ~TIOCGETA~ |       Mask | Len | Shift right | Value            |
|------+------------+------------+-----+-------------+------------------|
| NR   | 0x402c7413 |       0xff |   8 |           0 | 0x13 (19)        |
| TYPE | 0x402c7413 |     0xff00 |   8 |           8 | 0x74 (116 = 't') |
| SIZE | 0x402c7413 | 0x3FFF0000 |  14 |          16 | 0x2c (44)        |
| DIR  | 0x402C7413 | 0xC0000000 |   2 |          30 | 0x2 (IOC_READ)   |

From the results above, ~TYPE='t'~ and ~NR=19~ match the source for
~TIOCGETA~ discovered earlier.

The definition of the ~termios~ struct in is ~sys/sys/termios.h~

#+begin_src c
struct termios {
	tcflag_t	c_iflag;	/* input flags */
	tcflag_t	c_oflag;	/* output flags */
	tcflag_t	c_cflag;	/* control flags */
	tcflag_t	c_lflag;	/* local flags */
	cc_t		c_cc[NCCS];	/* control chars */
	int		c_ispeed;	/* input speed */
	int		c_ospeed;	/* output speed */
};
#+end_src

From that the size of the structure is 4 + 4 + 4 + 4 + 4 + 4 + 20
which is 44, matching the value for ~SIZE~ above.

Putting this into a short assembly language program (I use Nasm) to
test, with our newly validated value for ~TIOCGETA~.

#+begin_src asm
section .text
	
_start:
	mov rax, SYS_ioctl 	; % 54
	mov rdi, STDIN		; % 0
	mov rsi, 0x402c7413
	mov rdx, termios_c_iflag ; The beginning of the termios struct
	
	syscall

	jc error

exit:
	mov rdi, [termios_c_iflag]
	mov rax, 0x1
	syscall

error:
	mov rdi, rax	
	mov rax, 0x1
	syscall
#+end_src

Running in GDB and examining the struct memory after the syscall - 

#+begin_src
(gdb) x/4 0x2021e8
0x2021e8 <termios_c_iflag>:     0x00000300      0x00000007      0x00005b00      0x200005cf
#+end_src

Woot!  Success.  Now I can crack on with the second part of actually
setting the terminal attributes.


