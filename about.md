---
title: About
---
I am a technology generalist and leader, working from writing TCP
stacks in C through general ledgers and media streaming cloud based
services.  I have worked in engineering and technology roles in a
number of different industries, and have led teams of all sizes across
a variety of projects and domains.  This blog is a mix of technical
posts and writings about various technology subjects I've been
thinking about.
