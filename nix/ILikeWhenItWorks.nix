let

pkgs = import <nixpkgs> { };

in
{ TimmyW = pkgs.haskellPackages.callPackage ./default.nix { };
}
