{ mkDerivation, base, hakyll, hakyll-sass, stdenv }:
mkDerivation {
  pname = "TimmyW";
  version = "0.1.0.0";
  src = ../.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base hakyll hakyll-sass ];
  license = stdenv.lib.licenses.bsd3;
  hydraPlatforms = stdenv.lib.platforms.none;
}
